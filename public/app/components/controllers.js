function webHomeController($scope) {
    console.log("I am Home page Controller")
}
webHomeController.$inject = ["$scope"]

function selectionController($scope, $timeout, MessengerExtensions, optionsList, $location, botDataService) {
    console.log("Options Controller")
    JL().info("Options Selector Controller laoded with Messenger Extensions")
    var model = $scope.model = {}

    if (optionsList.type === 'occupation') {
        model.option_type = 'anzsco'
        model.description = "This is the list of all occupations that are currently eligible for migration to Australia. Its a consolidated list which inclues occuapotion for both long term and short term visas."
    } else if (optionsList.type === "189_others") {
        model.option_type = "option_id"
        model.description = "Please select which ever is applicable"
    }

    console.log(optionsList.type)
    model.option_type = optionsList.type
        //Set the header info
    MessengerExtensions.getUserID((uids) => {
        model.psid = uids.psid
        JL().info("Got PSID")
    }, (err, errMessage) => {

        JL().error("Cant get PSID")
        JL().error(err)
        JL().error(errMessage)
    })
    model.heading = "Select Occupation"
    model.optionsList = optionsList.list

    $scope.optionSelected = function(selectedOption) {
        model.selected = selectedOption
        console.log(selectedOption)
    }
    $scope.othersSelected = function(others) {

    }
    $scope.next = function() {
        model.waiting = true
        botDataService.postSelected(model.psid, model.selected)
    }
}
selectionController.$inject = ["$scope", '$timeout', 'MessengerExtensions', 'optionsList', '$location', 'botDataService']

function navigationController($scope, $location) {
    var model = $scope.model = {
        title: "My Visa"
    }
    switch ($location.path()) {
        case '/bot/fb/select/occupation':
            model.title = "Select your Occupation"
            break;
        case '/bot/fb/select/189_others':
            model.title = "Other Conditions"
            break;
        case '/bot/fb/learn_more':
            model.title = "Lean More"
            break;
    }
}
navigationController.$inject = ['$scope', '$location']