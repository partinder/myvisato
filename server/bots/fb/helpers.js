(function() {
    const settings = require("../../config/settings")();
    const request = require('request');

    module.exports = function() {
        var returnObj = {
            getUserProfile: getUserProfileFunc,
            validations: {
                email: emailValidationFunc,
                phone: phoneValidationFunc,
                trueFalse: trueFalseValidationFunc,
                number: numberValidationFunc,
                dob: dobValidationFunc
            },
            typingOn: typingOnFunc,
            errorMessage: errorMessageFunc,
            wrongAnswer: wrongAnswerFunc,
            textMessage: textMessageFunc,
            formatMessage: formatMessageFunc
        }
        return returnObj
            // Function Declarations

        function formatMessageFunc(event) {

            var senderID = event.sender.id;
            // var recipientID = event.recipient.id || null;
            // var timeOfMessage = event.timestamp || null;
            // var message = formatMessage(event);

            var returnMessage
            if (event.message) {
                if (event.message.text) {
                    returnMessage = {
                        type: "text",
                        payload: event.message.text
                    }
                }
                if (event.message.quick_reply) {
                    returnMessage = {
                        type: 'quick_reply',
                        payload: event.message.quick_reply.payload
                    }
                }
                if (event.message.attachments) {
                    returnMessage = {
                        type: 'attachments',
                        payload: event.message.attachments
                    }
                }
            }
            if (event.postback) {
                returnMessage = {
                    type: "postback",
                    payload: event.postback.payload
                }
            }
            if (event.webview) {
                returnMessage = {
                    type: "webview",
                    payload: event.webview.payload,
                    reply: event.reply
                }
            }

            return returnMessage
        }

        function textMessageFunc(message) {
            return {
                type: "text",
                payload: message
            }
        }

        function typingOnFunc() {
            return {
                type: 'sender_action',
                payload: 'typing_on'
            }
        }

        function errorMessageFunc() {
            return {
                type: 'text',
                payload: "Something is not right, we are working on it."
            }
        }

        function wrongAnswerFunc() {
            return {
                type: 'text',
                payload: "Sorry, I cant understand your answer, Please try again :"
            }
        }

        function getUserProfileFunc(userId, cb) {
            var options = {
                method: 'GET',
                uri: 'https://graph.facebook.com/v2.6/' + userId,
                qs: {
                    fields: 'first_name,last_name,profile_pic,locale,timezone,gender',
                    access_token: settings.fb.page_access_token
                }
            }

            request(options, function(err, res, body) {
                if (!err && res.statusCode === 200) {
                    console.log(body)
                    cb(null, body)
                } else {
                    cb(err)
                }
            })
        }

        function emailValidationFunc(message) {

            if (validateEmail(message)) {
                return message;
            }
            return null

            function validateEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
        }

        function phoneValidationFunc(message) {}

        function trueFalseValidationFunc(message) {
            console.log("Do validation for : ", message);
            if (message === "yes" || message === "no") {
                console.log("YES")
                return message
            }
            return null;
        }

        function numberValidationFunc(message) {
            var age = Number(message)
            console.log("user age is :", age)

            if (!isNaN(age)) {
                return message
            }
            console.log("returning null")
            return null
        }

        function dobValidationFunc(message) {
            if (isValidDate(message)) {
                return message
            }
            return null

            function isValidDate(dateString) {
                // First check for the pattern
                if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
                    return false;

                // Parse the date parts to integers
                var parts = dateString.split("/");
                var day = parseInt(parts[0], 10);
                var month = parseInt(parts[1], 10);
                var year = parseInt(parts[2], 10);

                // Check the ranges of month and year
                if (year < 1000 || year > 3000 || month == 0 || month > 12)
                    return false;

                var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

                // Adjust for leap years
                if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                    monthLength[1] = 29;

                // Check the range of the day
                return day > 0 && day <= monthLength[month - 1];
            };
        }
    }()
})()