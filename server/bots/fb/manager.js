(function() {
    const settings = require("../../config/settings")();
    const helpers = require("./helpers");
    const bot = require("../");
    const questionManager = require("./questionManager");

    var questionStack = settings.questionStackObj;
    var answers = settings.answersObj;

    exports = module.exports = function(event) {

        var message = helpers.formatMessage(event)
        var senderID = event.sender.id


        questionStack[senderID] = questionStack[senderID] || [];
        answers[senderID] = answers[senderID] || {};
        var replyMessages = [];
        replyMessages.push(helpers.typingOn());
        if (message.reply) {
            message.reply({ err: null });
        }
        switch (message.type) {
            case 'postback':
                switch (message.payload) {
                    case 'get_started_button':
                        // GET STARTED Called
                        helpers.getUserProfile(senderID, function(err, useData) {
                            userData = JSON.parse(useData);
                            replyMessages.push(helpers.textMessage("Welcome " + userData.first_name + " "+userData.last_name+". Welkcome to MyVisa - Your one stop Visa solution."))
                            replyMessages.push(questionManager.questionBank['get_started']['question'])
                            sendReply();
                        })
                        break;
                    case 'start_over':
                        //RESET AND START OVER
                        clearStacks();
                        replyMessages.push(helpers.textMessage('Sure, I am all set to start over again :)'));
                        sendReply();
                        break;
                    case 'query_intro':
                        // QUERY INTRO
                        clearStacks();
                        replyMessages = replyMessages.concat([helpers.textMessage("We would love to help you with your query, can you please provide us with a few details so we can better serve you")])
                            // UPDATE QUESTION STACK : Put Query Question Book on the User Question Stack
                        var queryQuestionBook = questionManager.getQuestionBook(message.payload);
                        questionStack[senderID] = queryQuestionBook;
                        console.info("Question stack after intro Query", questionStack[senderID])
                        processQuestionStack();
                        break;
                    case 'assessment_gsm':
                        clearStacks();
                        replyMessages = replyMessages.concat([helpers.textMessage("Lets start your assessment for Skilled Migration to Australia. This would take about 10 minutes.")])
                        var queryQuestionBook = questionManager.getQuestionBook(message.payload);
                        questionStack[senderID] = queryQuestionBook;
                        console.info("Quxestion stack after intro Query", questionStack[senderID])
                        processQuestionStack();
                        break;
                    case "stay_updated":
                        replyMessages = replyMessages.concat([
                            helpers.textMessage("Sure, We would love deliver to you the most recent changes to the Visa laws and immigration rules to help you stay updated."),
                            helpers.typingOn(),
                            helpers.textMessage("To help us send you the most relevant updates, please let us know a few things about you")
                        ])
                        sendReply();
                        break;
                    default:
                        console.log("default of payload")
                        messageManager(message)
                }
                break;
            default:
                messageManager(message)
        }

        function messageManager(message) {
            if (questionStack[senderID].length != 0) {
                // There are questions on the Stack so pass it onto the Stack Manager
                userQuestionStackManager();
            } else {
                // The bot hasnt asked a question. handle accordingly.
                if (message.type == "webview") {
                    replyMessages = replyMessages.concat([helpers.textMessage("Sorry, I cant understand that.")])
                } else {
                    replyMessages = replyMessages.concat([message]);
                }
                console.log(message)
                return sendReply();
            }
        }

        function userQuestionStackManager() {
            var lastQuestionOnStack = questionStack[senderID][0];
            var questionAsked = questionManager.questionBank[lastQuestionOnStack.questionID];

            console.log("Last Question on Stack :", lastQuestionOnStack);
            console.log("Question Asked", questionAsked);

            // FIRST : Check answer type
            if (questionAsked.answer_type.indexOf(message.type) != -1) {
                //SECOND : Validate Message
                var validatedMessage = message.payload;
                questionAsked.validations.forEach(function(validationFunc) {
                    validatedMessage = validationFunc(validatedMessage);
                })
                console.log("Validated Message : ", validatedMessage)
                if (validatedMessage) {
                    // VALID MESSAGE : Pop that question and find the next in line.
                    if (validatedMessage.anzsco) { // Q
                        replyMessages.push(helpers.textMessage("Selected Occupation : " + validatedMessage.name))
                    }
                    answers[senderID][lastQuestionOnStack.questionID] = validatedMessage;
                    questionStack[senderID].shift();
                    //THIRD : Check if there a secondary question to the the last question asked.
                    if (lastQuestionOnStack.next) {
                        console.log("Processing the next question")
                            // YES, get the next question and push it to question stack
                        lastQuestionOnStack.next(validatedMessage, function(err, nextQuestion) {
                            console.log(err, nextQuestion)
                            if (!err) {
                                if (nextQuestion) {
                                    nextQuestion.forEach(function(question) {
                                        questionStack[senderID].unshift(question);
                                    })

                                }
                                console.log("NEw Stack AFTER PUSH", questionStack[senderID])
                                processQuestionStack();
                            } else {
                                // Something wrng with fetching next question.
                                console.error("No Next Question in the Handle Message, Something is not right");
                                replyMessages = replyMessages.concat([helpers.errorMessage()])
                                sendReply();
                            }

                        })
                    } else {
                        // THIRD ERR : No next question, proceed to handle stack
                        console.log("No Next going to Processing ")
                        return processQuestionStack();
                    }

                } else {
                    // SECOND ERR: Message is not Validated
                    console.log("Not a Valid Message")
                    replyMessages = replyMessages.concat([helpers.wrongAnswer()])
                    processQuestionStack();
                }
            } else {
                // FIRST ERR : Message does not match the expeted answer type
                console.log("Answer Does not match answer type")
                replyMessages = replyMessages.concat([helpers.wrongAnswer()])
                processQuestionStack();
            }
        }

        function processQuestionStack() {
            // Check if something is on theQuestion Stack to process
            if (questionStack[senderID].length != 0) {
                // Put the last question on the stack in the reply Stream and send to user
                var lastQuestionOnStack = questionStack[senderID][0];
                console.log("Last Question on Stack : ", lastQuestionOnStack)
                if (lastQuestionOnStack.process_answers) {
                    //Process Answers 
                    replyMessages = replyMessages.concat(lastQuestionOnStack.process_answers(answers[senderID]))
                    clearStacks();
                    console.log("Stacks after clearing", questionStack[senderID])
                } else if (lastQuestionOnStack.questionID) {
                    var questionID = lastQuestionOnStack['questionID']
                    var questionToAsk = questionManager.questionBank[questionID];
                    console.log("Processing question and pushing on stack")
                    replyMessages = replyMessages.concat([questionToAsk["question"]])
                }
            } else {
                // Nothing is there, something is not wright.
                console.error("Trying to process a Question Stack which is empty");
                replyMessages = replyMessages.concat([helpers.errorMessage()]);
            }
            sendReply();
        }

        function clearStacks() {
            questionStack[senderID] = [];
            answers[senderID] = {};
            console.log("Cleared Stacks")
        }

        function sendReply() {
            console.log("Sending Message")
            bot.fb.send(senderID, replyMessages);
        }


    }
})()