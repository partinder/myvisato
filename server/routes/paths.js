(function() {
    const handlers = require('./handlers');
    const Boom = require("boom");
    const settings = require("../config/settings")();
    const JL = require('jsnlog').JL;
    const jsnlog_nodejs = require('jsnlog-nodejs').jsnlog_nodejs;
    module.exports = [{
        method: "GET",
        path: "/",
        handler: handlers.home
    }, {
        method: ["GET", "POST"],
        path: "/" + settings.fb.webhook_path,
        handler: handlers.fb.webhook,
        config: {
            ext: {
                onPreHandler: {
                    method: handlers.fb.webhookPre
                }
            }
        }
    }, {
        method: "POST",
        path: "/jsnlog.logger",
        handler: handlers.jsnlog
    }, {
        method: "GET",
        path: "/public/{params*}",
        handler: {
            directory: {
                path: "public",
                listing: false
            }
        }
    }, {
        method: '*',
        path: '/{p*}', // catch-all path
        handler: function(request, reply) {
            reply.file("public/index.html")
        }
    }, {
        method: "GET",
        path: "/api/v1/options_list",
        config: {
            auth: "api",
            handler: handlers.api.optionsList
        }
    }]
})()