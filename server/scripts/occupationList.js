(() => {
    const cheerio = require("cheerio");
    const request = require("request");
    const async = require("async");
    const mongoClient = require("mongodb").MongoClient;

    const occpationListUrls = [{
        parseType: '1',
        url: 'http://www.border.gov.au/Trav/Work/Work/Skills-assessment-and-assessing-authorities/skilled-occupations-lists/combined-stsol-mltssl'
    }];

    var asyncFuncs = [];
    occpationListUrls.forEach(function(url) {
        asyncFuncs.push(function(cb) {
            request.get(url.url, function(err, res, body) {
                if (!err)
                    cb(null, { parseType: url.parseType, html: body });
                else
                    cb(err);
            });
        });
    });
    console.log(asyncFuncs);
    async.series(asyncFuncs, function(err, results) {
        if (!err) {
            console.log("Got HTML, Start Parsing");
            var occupationsList = parseHTML(results);
            console.log("Parse HTML now save to DB");
            console.log(occupationsList[0]);
            //Put the results into DB
            saveToDB(occupationsList);
        } else {
            console.log("Some Error : ", err);
        }
    });

    function parseHTML(results) {
        var occupationList = [];
        results.forEach(function(result) {
            switch (result.parseType) {
                case '1':
                // Based on url as of date 11th JUNE 2017 : http://www.border.gov.au/Trav/Work/Work/Skills-assessment-and-assessing-authorities/skilled-occupations-lists/combined-stsol-mltssl
                    console.log("Parsing HTML with parse type 1");
                    const $ = cheerio.load(result.html);
                    $('table tbody tr').each(function(i, tr) {
                        if (i != 0) {
                            var occupationTR = $(this).children();
                            var occupation = {
                                name: $(occupationTR[0]).text().toString().trim().replace(/\u200B/g,'') || null,
                                anzsco: $(occupationTR[1]).text().toString().trim().replace(/\u200B/g,'') || null,
                                list_type : $(occupationTR[2]).text().toString().trim().replace(/\u200B/g,'') || null,
                                assesing_authority: $(occupationTR[3]).text().toString().trim().replace(/\u200B/g,'') || null
                            };
                            // console.log(occupation)
                            if(occupation.name && occupation.anzsco && occupation.assesing_authority){
                            	occupationList.push(occupation);
                            }
                        }
                    });
                    break;
            }
        });
        return occupationList;
    }

    function saveToDB(occupationsList){
    	var url = 'mongodb://localhost:27017/db_myvisa';
    	mongoClient.connect(url,function(err,db){
    		if(!err){
    			console.log("Connected to DB");
    			var occupations_col = db.collection("occupations");
    			occupations_col.insert(occupationsList,function(err,result){
    				if(!err){
    					console.log("%s records inserted",result.insertedCount);
    					process.exit(1);
    				} else {
    					console.log(err);
    				}
    			});
    		} else {
    			console.log(err);
    		}
    	});
    }
})();